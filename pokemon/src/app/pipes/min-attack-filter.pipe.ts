import { Pipe, PipeTransform } from '@angular/core';
import { Pokemon } from '../interfaces/pokemon';

@Pipe({
  name: 'minAttackFilter'
})
export class MinAttackFilterPipe implements PipeTransform {

  transform(pokemons: Array<Pokemon>, minAttack: number): Array<Pokemon> {
    if (minAttack >= 0) {
      return pokemons.filter(pokemon => pokemon.stats.attack >= minAttack);
    }
    else {
      return pokemons;
    }
  }

}
