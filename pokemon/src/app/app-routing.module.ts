import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoreComponent } from './components/store/store.component';
import { DeckComponent } from './components/deck/deck.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { AuthGuardGuard } from './service/guard/auth-guard.guard';


const routes: Routes = [
{ path: '', redirectTo: 'login', pathMatch: 'full' },
{  path: 'login', component: LoginPageComponent },
{  path: 'store', canActivate:[AuthGuardGuard], component: StoreComponent },
{  path: 'deck', canActivate:[AuthGuardGuard], component: DeckComponent },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
