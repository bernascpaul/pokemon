export interface Pokemon {
    poke_id: number;
    name: string;
    image: string;
    type: string;
    stats: {
        attack: number;
        hp: number;
        speed: number;
        defense: number;
    }
}
