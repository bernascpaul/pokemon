export interface User {
    name: string,
    deck: number[],
    _id: string,
    coins: number,
    token: string,
    connected: boolean
}
