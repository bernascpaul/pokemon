import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Pokemon } from 'src/app/interfaces/pokemon';
import { UserService } from '../../service/user/user.service';
import { User } from '../../interfaces/user';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {
  @Input() pokemon: Pokemon;
  @Output() addPokemon: EventEmitter<Pokemon> = new EventEmitter();
  constructor(
    private userService:UserService,
  ) { }

  addPoke(pokemon: Pokemon) {
    this.addPokemon.emit(pokemon);
  }

  ngOnInit() {
  }

}
