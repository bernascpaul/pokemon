import { Component, OnInit, Input } from '@angular/core';
import { PokemonService } from '../../service/pokemon/pokemon.service';
import { Pokemon } from '../../interfaces/pokemon';
import { UserService } from '../../service/user/user.service';
import { User } from '../../interfaces/user';
import { Router } from'@angular/router';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {
  pokemons:Array<Pokemon> = [];
  user:User = this.UserService.getUser();

  @Input() pokemon: Pokemon;

  constructor(
    private PokemonService: PokemonService,
    private UserService: UserService,
    private router:Router
  ) { }

  getPokemons() {
    if(this.user.coins >= 10) {
      this.pokemons = [];
      for(let i = 0; i < 10; i++) {
        this.PokemonService.getPokemonsById(Math.floor(Math.random() * 200) + 1).then(((data: Pokemon) => {
        this.pokemons.push(data);
      }));
    }
    this.user.coins -= 10;
    this.UserService.updateUser(this.user);
    }    
  }
  
  addPokemon(pokemon: Pokemon) {
    this.user.deck.push(pokemon.poke_id);
    this.UserService.setUser(this.user);
    this.UserService.updateUser(this.user);
    this.pokemons = this.pokemons.filter(p => p.poke_id !== pokemon.poke_id);
  }

  deletePokemon(pokemon: Pokemon) {
    this.pokemons = this.pokemons.filter(p => p.poke_id !== pokemon.poke_id);
    this.user.coins += 1;
  }

  ngOnInit() { }

}
