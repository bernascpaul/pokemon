import { Component, OnInit, Input } from '@angular/core';
import { Router } from'@angular/router';
import { UserService } from '../../service/user/user.service';
import { User } from '../../interfaces/user';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  user:User;
  constructor(
    private UserService:UserService,
    private router:Router
  ) { }

  deleteUserByToken() {
    this.user = this.UserService.getUser();
    this.UserService.deleteUser(this.user.token);
    this.router.navigate(['/login']);
  }

  ngOnInit() {
    this.user = this.UserService.getUser();
  }
}
