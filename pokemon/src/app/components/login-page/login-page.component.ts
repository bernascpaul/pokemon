import { Component, OnInit } from '@angular/core';
import { Router } from'@angular/router';
import { UserService } from '../../service/user/user.service';
import { User } from '../../interfaces/user';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  user: User = this.UserService.getUser();
  login: string;
  model:{name: string};
  constructor(
    private UserService:UserService,
    private router:Router,
  ) 
  {
    this.model = {name:""};
  }

  submitInscription() {
    this.UserService.postUserByName(this.model.name).then(((data:any) => {
      var token = data.token;
      this.UserService.getUserByToken(token).then(((data: User) => {
        let user: User = {
          'name': data["name"],
          'deck': data["deck"],
          'coins': data["coins"],
          '_id': data["_id"],
          'token': token,
          'connected': true
        };
        this.UserService.setUser(user);
        this.router.navigate(['/deck']);
      }));
  }));
  }

  ngOnInit() { }
}
