import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../../service/pokemon/pokemon.service';
import { Pokemon } from '../../interfaces/pokemon';
import { UserService } from '../../service/user/user.service';
import { User } from '../../interfaces/user';
import { Router } from'@angular/router';

@Component({
  selector: 'app-deck',
  templateUrl: './deck.component.html',
  styleUrls: ['./deck.component.css']
})
export class DeckComponent implements OnInit {
  pokemons:Array<Pokemon> = [];
  user:User = this.userService.getUser();
  minAttack: number;
  typePokemon: string;

  constructor(
    private userService: UserService,
    private PokemonService:PokemonService,
    private router:Router
  ) { }

  keyUpMinAttack($minAttack) {
    this.minAttack = $minAttack;
  }

  ngOnInit() {
    this.user.deck.forEach(pokemon => 
      this.PokemonService.getPokemonsById(pokemon).then(((data:Pokemon) => {
        this.pokemons.push(data);
      }))
    );
  }
}
