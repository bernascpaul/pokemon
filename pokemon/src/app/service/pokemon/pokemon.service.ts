import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  appUrl = 'https://lpweblannion.herokuapp.com/api/pokemon/';

  constructor(
    private _http: HttpClient,
  ) { }

  getPokemonsById(id:number) {
    return this._http.get(this.appUrl+id).toPromise();
  }
}
