import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { User } from 'src/app/interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  url:string = 'https://lpweblannion.herokuapp.com/api/user';
  user:User;
  
  constructor(
    private _http:HttpClient,
    private userService: UserService
  ) {
    this.user = {
      'name': "",
      'deck': [],
      'coins': 100,
      '_id': "",
      'token': "",
      'connected': false
    };
   }
   

  postUserByName(name:string) {
    return this._http.post(this.url + '/login', {'name': name}).toPromise();
  }

  getUserByToken(token:string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'my-token',
        'Content-Type':  'application/json',
      })
    };

    return this._http.get(this.url, {
      headers: httpOptions.headers.set('Authorization', token),
    }).toPromise();
  }

  deleteUser(token:string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'my-token',
        'Content-Type':  'application/json',
      })
    };

    return this._http.delete(this.url, {
      headers: httpOptions.headers.set('Authorization', token),
    }).toPromise();
  }

  updateUser(user:User) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'my-token',
        'Content-Type':  'application/json',
      })
    };

    return this._http.put(this.url,  {'coins': user.coins, 'deck': user.deck}, {
      headers: httpOptions.headers.set('Authorization', user.token),
    }).toPromise();
  }

  getUser() {
    return this.user;
  }

  setUser(user:User) {
    return this.user = user;
  }

  logIn() {
    return this.user.connected = true; 
  }

  logOut() {
    return this.user.connected = false;
  }
}
