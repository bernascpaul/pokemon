import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './components/menu/menu.component';
import { CardsComponent } from './components/cards/cards.component';

import { HttpClientModule } from '@angular/common/http';
import { PokemonService } from './service/pokemon/pokemon.service';
import { StoreComponent } from './components/store/store.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { DeckComponent } from './components/deck/deck.component';
import { MinAttackFilterPipe } from './pipes/min-attack-filter.pipe';
import { AuthGuardGuard } from './service/guard/auth-guard.guard';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    CardsComponent,
    StoreComponent,
    LoginPageComponent,
    DeckComponent,
    MinAttackFilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [PokemonService, AuthGuardGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
