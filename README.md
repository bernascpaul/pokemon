# Pokemon

Projet Pokemon Angular

# Authors

- Cynthia Forestier
- Paul Bernasconi

# Connexion au serveur et démarrage de l'application

- ssh -L 4200:localhost:4200 root@nom.lpweb-lannion.fr
- ng serve

# Composants

- cards
- menu
